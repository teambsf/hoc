﻿<?php
header('Content-Type: text/html; charset=utf-8');
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/Berlin');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/PHPExcel.php';

$inputFileName = './texte-hochzeitskarten.xlsx';

/** Load $inputFileName to a PHPExcel Object  **/
$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    $worksheets[$worksheet->getTitle()] = $worksheet->toArray();
}
 
$data = array();
$u = array();
foreach ($worksheets as $w => $d)
{
	foreach ($d as $r => $c)
	{
		//echo '-'.$r.'-<br>';
		if ($r != 0)
		{
			$x = array();
			foreach ($c as $a => $b)
			{
				//echo $a .'<br>';
				$e = explode(',',$b);
				if (count($e) > 1)
				{
					//array_walk($e, 'trim_value');
					$ee = array_map('trim', $e);
					$b = $ee;
				}
				$var = preg_replace("/[^A-Za-z0-9]/", "", $worksheets[$w][0][$a]);
				$x[$var] = $b;
				//$u[$var][$b] = $b;
			}
			
			$x['Filters'] = makefilter($x);
			$data[$c[0]] = $x;
			$x=array();
		}
	}
}
//echo '<pre>';
//print_r($u);
//echo '</pre>';
echo '<pre>';
print_r($data);
echo '</pre>';
write_file('data.json',json_encode($data));


function write_file($output_file, $data)
{
	$ifp = fopen($output_file, "wb"); 

	//$data = explode('\n', $base64_string);
	fwrite($ifp, $data); 
	fclose($ifp); 
}

function makefilter($data)
{
	/*
	    [filterTyp] => Array
        (
            [Einladungskarte] => Einladungskarte
            [savethedate] => savethedate
            [Dankeskarte] => Dankeskarte
            [Menükarte] => Menükarte
            [Kirchenheft] => Kirchenheft
        )

    [filterKategorie] => Array
        (
            [Vintage] => Vintage
            [Klassisch] => Klassisch
            [Modern] => Modern
            [Verspielt] => Verspielt
        )
		*/
	$filters = array( 
		"einladungskarte"	=> ($data['filterTyp'] == 'Einladungskarte' ? true : false),
		"savethedate"		=> ($data['filterTyp'] == 'savethedate' ? true : false),
		"dankeskarte"		=> ($data['filterTyp'] == 'Dankeskarte' ? true : false),
		"menue"				=> ($data['filterTyp'] == 'Menükarte' ? true : false),
		"kirchenhefte"		=> ($data['filterTyp'] == 'Kirchenheft' ? true : false),
		"vintage"			=> ($data['filterKategorie'] == 'Vintage' ? true : false),
		"modern"			=> ($data['filterKategorie'] == 'Klassisch' ? true : false),
		"klassisch"			=> ($data['filterKategorie'] == 'Modern' ? true : false),
		"verspielte"		=> ($data['filterKategorie'] == 'Verspielt' ? true : false),
	);
	
	return $filters;
}




































