(function () {
	'use strict';

	angular
	.module('hoc')
	.constant('moment', moment)
	.controller('AppCtrl', AppCtrl)
	.service('PageTitle', function () {
		var title = 'Productmate';
		return {
			title : function () {
				return title;
			},
			setTitle : function (newTitle) {
				title = newTitle;
			}
		};
	})
	.service('MetaInformation', function () {
		var metaDescription = '';
		var metaKeywords = '';
		return {
			metaDescription : function () {
				return metaDescription;
			},
			metaKeywords : function () {
				return metaKeywords;
			},
			reset : function () {
				metaDescription = '';
				metaKeywords = '';
			},
			setMetaDescription : function (newMetaDescription) {
				metaDescription = newMetaDescription;
			},
			appendMetaKeywords : function (newKeywords) {
				for (var key in newKeywords) {
					if (metaKeywords === '') {
						metaKeywords += newKeywords[key].name;
					} else {
						metaKeywords += ', ' + newKeywords[key].name;
					}
				}
			}
		};
	})
	
	.run(['$templateCache', function ($templateCache) {
				$templateCache.put('partials/menu-toggle.tmpl.html',
					'<md-button class="md-button-toggle"\n' +
					'  ng-click="toggle()"\n' +
					'  aria-controls="docs-menu-{{section.name | nospace}}"\n' +
					'  flex layout="row"\n' +
					'  aria-expanded="{{isOpen()}}">\n' +
					'  {{section.name}}\n' +
					'  <span aria-hidden="true" class=" pull-right fa fa-chevron-down md-toggle-icon"\n' +
					'  ng-class="{\'toggled\' : isOpen()}"></span>\n' +
					'</md-button>\n' +
					'<ul ng-show="isOpen()" id="docs-menu-{{section.name | nospace}}" class="menu-toggle-list">\n' +
					'  <li ng-repeat="page in section.pages">\n' +
					'    <menu-link section="page"></menu-link>\n' +
					'  </li>\n' +
					'</ul>\n' +
					'');
			}
		])
	.directive('menuToggle', ['$timeout', function ($timeout) {
				return {
					scope : {
						section : '='
					},
					templateUrl : 'partials/menu-toggle.tmpl.html',
					link : function (scope, element) {
						var controller = element.parent().controller();

						scope.isOpen = function () {
							return controller.isOpen(scope.section);
						};
						scope.toggle = function () {
							controller.toggleOpen(scope.section);
						};

						var parentNode = element[0].parentNode.parentNode.parentNode;
						if (parentNode.classList.contains('parent-list-item')) {
							var heading = parentNode.querySelector('h2');
							element[0].firstChild.setAttribute('aria-describedby', heading.id);
						}
					}
				};
			}
		])
	.factory('menu', [
			'$location',
			'$rootScope',
			function ($location) {

				var sections = [{
						name : 'Home',
						state : 'home',
						type : 'link'
					}, {
						name : 'Datenschutz',
						state : 'datenschutz',
						type : 'link'
					}, {
						name : 'Hochzeitskarten',
						state : 'hochzeitskarten',
						type : 'link'
					}, {
						name : 'Impressum',
						state : 'impressum',
						type : 'link'
					}, {
						name : 'Startseite',
						state : 'startseite',
						type : 'link'
					}, {
						name : 'Kontakt',
						state : 'kontakt',
						type : 'link'
					}, {
						name : 'Hello',
						state : 'hello',
						type : 'link'
					}
				];
				var self;
				return self = {
					sections : sections,
					toggleSelectSection : function (section) {
						self.openedSection = (self.openedSection === section ? null : section);
					},
					isSectionSelected : function (section) {
						return self.openedSection === section;
					},
					selectPage : function (section, page) {
						page && page.url && $location.path(page.url);
						self.currentSection = section;
						self.currentPage = page;
					}
				};
				function sortByHumanName(a, b) {
					return (a.humanName < b.humanName) ? -1 :
					(a.humanName > b.humanName) ? 1 : 0;
				}
			}
		]);

	function AppCtrl($http, $scope, $state, $filter, PageTitle, $urlRouter, menu) {
		var vm = this;
		//console.log($state);
		$state.view = 'main';
		PageTitle.setTitle('damn im good');
		$scope.PageTitle = PageTitle;
		//console.log(menu);
		//functions for menu-link and menu-toggle
		vm.isOpen = isOpen;
		vm.toggleOpen = toggleOpen;
		vm.autoFocusContent = false;
		$scope.menu = menu;

		vm.status = {
			isFirstOpen : true,
			isFirstDisabled : false
		};

		function isOpen(section) {
			return menu.isSectionSelected(section);
		}

		function toggleOpen(section) {
			menu.toggleSelectSection(section);
		}
	}

	angular
	.module('hoc')
	.config(config)
	.config(function ($mdThemingProvider) {

		$mdThemingProvider.definePalette('bsf-palette', {
			'50' : 'ffffff',
			'100' : 'ffcdd2',
			'200' : 'ef9a9a',
			'300' : 'e57373',
			'400' : 'ef5350',
			'500' : 'cd0100',
			'600' : 'e53935',
			'700' : 'd32f2f',
			'800' : 'C62828',
			'900' : 'B71C1C',
			'A100' : '263238',
			'A200' : 'ff5252',
			'A400' : 'ff1744',
			'A700' : 'd50000',
			'contrastDefaultColor' : 'light', // whether, by default, text (contrast)
			// on this palette should be dark or light
			'contrastDarkColors' : ['50', '100', //hues which contrast should be 'dark' by default
				'200', '300', '400', 'A100'],
			'contrastLightColors' : undefined // could also specify this if default was 'dark'
		});

		$mdThemingProvider.definePalette('bsf-bg', {
			'50' : 'ffffff',
			'100' : 'F5F5F5',
			'200' : 'EEEEEE',
			'300' : 'E0E0E0',
			'400' : 'BDBDBD',
			'500' : 'CFD8DC',
			'600' : '757575',
			'700' : '616161',
			'800' : '424242',
			'900' : 'b71c1c',
			'A100' : '455A64',
			'A200' : 'ff5252',
			'A400' : 'ff1744',
			'A700' : '212121',
			'contrastDefaultColor' : 'light', // whether, by default, text (contrast)
			// on this palette should be dark or light
			'contrastDarkColors' : ['50', '100', //hues which contrast should be 'dark' by default
				'200', '300', '400', 'A100'],
			'contrastLightColors' : undefined // could also specify this if default was 'dark'
		});

		$mdThemingProvider.theme('default')
		.primaryPalette('blue-grey')
		.backgroundPalette('grey')
		.accentPalette('orange')
		.warnPalette('red');
		$mdThemingProvider.theme('bsfnova')
		.primaryPalette('bsf-bg');
		/*
		.backgroundPalette('black')
		.accentPalette('green')
		.warnPalette('yellow');*/
	});

	/** @ngInject */
	function config($logProvider) {
		// Enable log
		$logProvider.debugEnabled(true);
	}

})();
