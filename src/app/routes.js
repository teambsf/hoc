(function () {
	'use strict';

	angular
	.module('hoc')
	.config(routesConfig);

	/** @ngInject */
	function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
		$locationProvider.html5Mode(true).hashPrefix('!');
		$urlRouterProvider.otherwise('/');

		$stateProvider
		.state('home', {
			url : '/',
			views : {
				"main" : {
					templateUrl : 'template-parts/home.html',
					controller : 'homeCtrl',
				},
			}
		})
		.state('datenschutz', {
			url : '/datenschutz',
			views : {
				"main" : {
					templateUrl : 'template-parts/datenschutz.html',
					controller : 'datenschutzCtrl',
				},
			}
		})
		.state('hochzeitskarten', {
			url : '/hochzeitskarten',
			views : {
				"main" : {
					templateUrl : 'template-parts/hochzeitskarten.html',
					controller : 'hochzeitskartenCtrl',
				},
			}
		})
		.state('impressum', {
			url : '/impressum',
			views : {
				"main" : {
					templateUrl : 'template-parts/impressum.html',
					controller : 'impressumCtrl',
				},
			}
		})
		.state('kontakt', {
			url : '/kontakt',
			views : {
				"main" : {
					templateUrl : 'template-parts/kontakt.html',
					controller : 'kontaktCtrl',
				},
			}
		})
		.state('startseite', {
			url : '/startseite',
			views : {
				"main" : {
					templateUrl : 'template-parts/startseite.html',
					controller : 'startseiteCtrl',
				},
			}
		})
		.state('hello', {
			url : '/hello',
			views : {
				"main" : {
					templateUrl : 'template-parts/hello.html',
					controller : 'helloCtrl',
				},
			}
		});

	}
})();
