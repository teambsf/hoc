(function () {
	'use strict';

	angular
	.module('hoc', ['ngAnimate', 'ngCookies', 'ngSanitize', 'ngResource', 'ui.router', 'ui.utils', 'ui.router.title', 'ngMaterial']);

})();
