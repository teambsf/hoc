(function () {
	'use strict';

	angular
	.module('hoc')
	.controller('hochzeitskartenCtrl', hochzeitskartenCtrl);

	function hochzeitskartenCtrl($http, $scope, $state, $q, $filter, PageTitle, $urlRouter, menu) {
		var vm = this;

		PageTitle.setTitle('Joes awesome title');
		console.log($state);
		$scope.ifilters = {
			einladungskarte : true,
			savethedate : true,
			dankeskarte : true,
			menue : true,
			kirchenhefte : true,
			vintage : true,
			modern : true,
			klassisch : true,
			verspielte : true
		};

		/*
		var itemfx = {
		einladungskarte : 0,
		savethedate : 0,
		dankeskarte : 0,
		menue : 0,
		kirchenhefte : 0,
		vintage : 0,
		modern : 0,
		klassisch : 0,
		verspielte : 0,
		};
		 */
		var itemfx = {
			einladungskarte : 0,
			savethedate : 0,
			dankeskarte : 0,
			menue : 0,
			kirchenhefte : 0,
			vintage : 0,
			modern : 0,
			klassisch : 0,
			verspielte : 0
		};
		$scope.todos = {};
		$scope.todostemp = [];
		$scope.filteredtodos = [];
		$scope.master = [];
		$scope.currentPage = 1;
		$scope.totallistings = 0;

		$state.view = 'main';
		//$scope.product_list_1 = $http.get('data.json', {cache: false});
		$http.get('data.json', {
			cache : false
		}).then(function (res) {

			$scope.todos = res.data;
			$scope.todostemp = $scope.todos;

			angular.forEach($scope.todostemp, function (prop, key) {

				angular.forEach(itemfx, function (fv, fk) {
					if (prop.Filters[fk] === true) {
						itemfx[fk] = ((itemfx[fk] || 0) + 1);
					}

				});

			});

			$scope.master = res.data;
			$scope.filteredtodos = res.data;
			$scope.itemf = itemfx;
			$scope.totallistings = $scope.filteredtodos.length;

		});

		$scope.$watch('ifilters', function (a, b) {
			if (typeof a != "undefined") {
				console.log('a:', a);
				console.log('b:', b);
				var tmp1 = [];
				var tmp2 = [];

				tmp1 = $scope.master;
				//tmp2 = $filter('itemFilterArray')(tmp1, a, 'Filters');
				tmp2 = $filter('inFilterArrayx')(tmp1, a, 'Filters');
				//console.log('filtered length ',tmp2.length);
				$scope.filteredtodos = tmp2; //$scope.master;
				//$scope.totallistings = $scope.filteredtodos.length;
			}
			if (typeof a === "undefined") {
				$scope.filteredtodos = $scope.master;
			}
		}, true);

	}

})();
