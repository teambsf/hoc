(function () {
	'use strict';

	angular
	.module('hoc')
	// page filters hochz
	.filter('itemFilterArray', function ($filter) {
		return function (list, a, element) {
			//var a = ['einladungskarte','savethedate','dankeskarte','menue','kirchenhefte','vintage','modern','klassisch','verspielte'];
			var data1 = [];
			var data2 = [];
			var data3 = [];
			
			angular.forEach(a, function (value, key) {
				if (!value) {
					list = $filter('filter')(list, {
							Filters : {
								key : true
							}
						});
				}
			});

			return list;
		}
	})
	.filter('inFilterArrayx', function ($filter) {
		return function (list, a, element) {
			var data = [];
			var tx = true;
			
			angular.forEach(list, function (itemx, ID) {
				tx = true;
				angular.forEach(a, function (value, key) {
					if (!value) {
						if (itemx.Filters[key] === true) {
							tx = false;
						}
					}
				});
				if (tx)
				{
					data.push(itemx);
				}
			});
			
			return data;
		};
	})
	.filter('inFilterArray', function ($filter) {
		return function (list, arrayFilter, element) {
			if (arrayFilter) {
				if (element) {
					return $filter("filter")(list, function (listItem) {
						return arrayFilter.indexOf(listItem[element]) != -1;
					});
				} else {
					return $filter("filter")(list, function (listItem) {
						return arrayFilter.indexOf(listItem) != -1;
					});
				}
			}
		}
	})
	.filter('unique', function () {

		return function (arr, field) {
			var o = {},
			i,
			l = arr.length,
			r = [];
			for (i = 0; i < l; i += 1) {
				o[arr[i][field]] = arr[i];
			}
			for (i in o) {
				r.push(o[i]);
			}
			return r;
		};
	})

	//take all whitespace out of string
	.filter('nospace', function () {
		return function (value) {
			return (!value) ? '' : value.replace(/ /g, '');
		};
	})
	//replace uppercase to regular case
	.filter('humanizeDoc', function () {
		return function (doc) {
			if (!doc)
				return;
			if (doc.type === 'directive') {
				return doc.name.replace(/([A-Z])/g, function ($1) {
					return '-' + $1.toLowerCase();
				});
			}

			return doc.label || doc.name;
		};
	});

	
})();