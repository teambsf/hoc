<?php
$path_length     = strrpos(__FILE__, "/");
$this_directory2 = '/var/www/vhosts/relaunch.bsf-immobilien.de/httpdocs/';
$this_directory = substr(__FILE__, 0, $path_length).'/';


$wohnen = array();
$gewerbe = array();
$kapital = array();

$s1 = file_get_contents ($this_directory.'properties/homepage.json');
$data1 = json_decode($s1, true);
$s2 = file_get_contents ($this_directory.'properties/master.json');
$data2 = json_decode($s2, true);
//echo json_encode($data2);
$ids = array();
$data = array();

foreach($data1 as $r1 => $e1)
{
	$data[$e1['id']] = $e1;
}
foreach($data2 as $r2 => $e2)
{
	$data[$e2['id']] = $e2;
}
foreach ($data as $i => $c)
{
	unset($c['common.publishChannels']);
	if (isset($c['apiSearchData']['searchField1']))
	{
		echo $c['apiSearchData']['searchField1'].'<br>';
		if ($c['apiSearchData']['searchField1'] == 'kapital')
		{
			$kapital[] = $c;
			//copy($this_directory.'properties/'.$c['id'].'.json', $this_directory.'properties/kapital/'.$c['id'].'.json');
		}
		if ($c['apiSearchData']['searchField1'] == 'wohnen')
		{
			$wohnen[] = $c;
			//copy($this_directory.'properties/'.$c['id'].'.json', $this_directory.'properties/wohnen/'.$c['id'].'.json');
		}
		if ($c['apiSearchData']['searchField1'] == 'gewerbe')
		{
			$gewerbe[] = $c;
			//copy($this_directory.'properties/'.$c['id'].'.json', $this_directory.'properties/gewerbe/'.$c['id'].'.json');
		}
		
	}
	if ($c["realEstateState"] == "ACTIVE")
	{
		if ($c['xsitype'] == 'OfferApartmentRent' OR $c['xsitype'] == 'OfferHouseRent' OR $c['xsitype'] == 'OfferApartmentBuy' OR $c['xsitype'] == 'OfferHouseBuy' OR $c['xsitype'] == 'OfferShortTermAccommodation')
		{
			$wohnen[] = $c;
		}
		if ($c['xsitype'] == "OfferIndustry" OR $c['xsitype'] == "OfferOffice" OR $c['xsitype'] == "OfferStore" OR $c['xsitype'] == "OfferGastronomy" OR $c['xsitype'] == "OfferTradeSite" OR $c['xsitype'] == "OfferSpecialPurpose")
		{
			$gewerbe[] = $c;
		}
	}
}
//write_file($this_directory.'properties/gewerbe.json', json_encode($gewerbe));
//write_file($this_directory.'properties/wohnen.json', json_encode($wohnen));
//write_file($this_directory.'properties/kapital.json', json_encode($kapital));
echo 'wohnen '.count($wohnen).'<br>';
echo 'gewerbe '.count($gewerbe).'<br>';
echo 'kapital '.count($kapital).'<br>';


?>